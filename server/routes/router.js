const express = require('express');
const Data = require('../models/data.schema.js');

const router = express.Router();

router.get('/', (req,res) => {
  console.log("router.get");

  Data.find({}).then((data) => {
    console.log(data);
    res.send(data);
  }).catch((err) => {
    console.log(err);
    res.sendStatus(500);
  });
  
});

router.post('/', (req,res) => {
  console.log("router.post");

  let myData = new Data(req.body);

  myData.save().then((data) => {
    res.sendStatus(201);
  }).catch((err) => {
    console.log(err);
    res.sendStatus(500);
  });
    
});

module.exports = router;
