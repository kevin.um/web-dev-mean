const mongoose = require("mongoose");

let Schema = mongoose.Schema;

let dataSchema = new Schema({
  inputVal: {type: String}
});

module.exports = mongoose.model("Data", dataSchema);
