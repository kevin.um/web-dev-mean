const express = require("express");
const mongoose= require("mongoose");
const routes  = require("./routes/router.js");
const bodyParser = require("body-parser");

const app = express();
const PORT = process.env.PORT || 5000;

app.use(express.static("server/public"));
app.use(bodyParser.urlencoded({extended:true}));
app.use('/home', routes);

app.listen(PORT, function(){
    console.log("listening on PORT", PORT);
});

// Mongoose
const databaseURL = "mongodb://localhost:27017/datadb";
mongoose.connect(databaseURL);
mongoose.connection.on("connected",()=>{
    console.log("mongoose connected to database URL");
});
mongoose.connection.on("error", (err) => {
    console.log("mongooses connection error", err);
});

